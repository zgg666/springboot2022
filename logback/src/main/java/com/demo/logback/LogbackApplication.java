package com.demo.logback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author by Erica
 * @date 2022/2/13 4:49 PM
 * Desc:
 */
@SpringBootApplication
public class LogbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogbackApplication.class,args);
    }
}
