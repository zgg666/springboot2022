package com.demo.logback.controller;

import org.apache.juli.logging.LogFactory;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

/**
 * @author by Erica
 * @date 2022/2/13 4:49 PM
 * Desc:
 */
@RestController
public class TestLogController {

    private final static Logger logger= LoggerFactory.getLogger(TestLogController.class);

    @GetMapping
    public String testLog(){
        logger.info("test my log to file");
        return "hello,log";
    }
}
