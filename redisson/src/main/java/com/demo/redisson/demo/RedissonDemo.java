package com.demo.redisson.demo;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import javax.annotation.Resource;

/**
 * @author by Erica
 * @date 2022/1/10 9:04 PM
 * Desc:
 */
public class RedissonDemo {

    @Resource
    private RedissonClient redissonClient;

    public void testLock() {
        RLock lock = redissonClient.getLock("hello");
        lock.lock();
        try {

            //todo

        } finally {
            lock.unlock();
        }
    }


}
